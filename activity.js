//#3 inster one
db.hotel.insertOne({
	"name": "single",
	"accomodates": 2,
	"price": "1000",
	"description": "A simple room with all the basic necessities",
	"rooms_available": 10,
	"isavailable": "false"
});


//#4 insert many
db.hotel.insertMany([
		
		{
			"name": "double",
			"accomodates": 3,
			"price": "2000",
			"description": "A room fit for a small family going on a vacation",
			"rooms_available": 5,
			"isavailable": "false"
		},

		{
			"name": "queen",
			"accomodates": 4,
			"price": "4000",
			"description": "A room with a queen sized bed perfect for a simple getaway",
			"rooms_available": 15,
			"isavailable": "false"
		},
]);

//#5 find
db.hotel.find({name:"double"});

//#6 update one
db.hotel.updateOne(

	{name: "queen"}, 
	
	{$set:{rooms_available: 0}}
);

//#7. Delete rooms that have 0 available rooms

db.hotel.deleteMany(
	{
		rooms_available: 0
	}
);